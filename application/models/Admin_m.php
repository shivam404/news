<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_m extends CI_Model{
	public function submit(){
		$field1=array(
			'username'=>$this->input->post('fullname'),
			'email'=>$this->input->post('email'),
			'password'=>$this->input->post('password'),
			
		);
		
		$this->db->insert('User',$field1);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	}
	 public function validate($username,$password){
     $query = $this->db->query("SELECT * FROM   admin Where username='$username' && password='$password'");
       if($query->num_rows() == 1)
        {
			$row = $query->row();
            $data = array(
                    'email' => $row->email,
                    'username' =>$row->username,
                   
                    );
            $this->session->set_userdata($data);
            return true;
        }
            return false;
    }
    public function addnews(){
    	
    	
		$field1=array(
			'title'=>$this->input->post('title'),
			'source_link'=>$this->input->post('source_link'),
			'category_id'=>$this->input->post('category'),
			'content'=>$this->input->post('content'),
			);
		
		$this->db->insert('news',$field1);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	}
	public function addadvertisement(){
    	
    	
		$field=array(
			
			'link'=>$this->input->post('source_link'),
			'category_id'=>$this->input->post('category'),
			
			);

		
		$this->db->insert('ads',$field);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	}
	public function getBlog(){
        $query = $this->db->get('news');
        if($query->num_rows()>0){
            return $query->result();

        }
        else{
            return false;
        }
    }
    public function fetch_category(){
        $query = $this->db->get('category');
        if($query->num_rows()>0){
            return $query->result();

        }
        else{
            return false;
        }
    }

    public function viewadd(){
        $query = $this->db->query("select * from news 
                                INNER  JOIN  category 
                                on category.id = news.category_id");
        if($query->num_rows()>0){
            return $query->result();

        }
        else{
            return false;
        }
    }
    function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }
    public function getBlogByID($id){
    	
		$this->db->where('id',$id);
		$query = $this->db->get('news');
		if($query->num_rows()>0){
			return $query->row();

		}
		else{
			return false;
		}
	}
	public function getBlogid($id){
    	
		$this->db->where('id',$id);
		$query = $this->db->get('ads');
		if($query->num_rows()>0){
			return $query->row();

		}
		else{
			return false;
		}
	}
	public function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('news');
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	}
	public function deleteadd($id){
		$this->db->where('id',$id);
		$this->db->delete('ads');
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	}
	public function update(){
		$id = $this->input->post('txt_hidden');
			
		$field=array(
			'title'=>$this->input->post('title'),
			'source_link'=>$this->input->post('source_link'),
			'category_id'=>$this->input->post('category'),
			'content'=>$this->input->post('content'),
			
		);
			$this->db->where('id',$id);
			$this->db->update('news',$field);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	
	}
	public function updateadd(){
		$id = $this->input->post('txt_hidden');
			
		$field=array(
			
			'link'=>$this->input->post('source_link'),
			'category_id'=>$this->input->post('category'),
			
			
		);
			$this->db->where('id',$id);
			$this->db->update('ads',$field);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	
	}
	
	
}
?>
