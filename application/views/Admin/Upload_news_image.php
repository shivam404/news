<div class="tab-content">
    <?php
    if(isset($msg))
    {
        echo $msg;
    }
    if(isset($img_error))
    {
        foreach ($img_error as $error)
        {
            echo $error;
        }
    }
    ?>
    <div class="tab-pane active" id="tab_0">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add News
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">

                <!-- BEGIN FORM-->
                <form action="<?php echo base_url('admin/upload_image/').$upload_id; ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Select no of image</label>
                            <div class="col-md-6">
                               <select name="image_count" class="form-control input-circle" id="image_count">
                                  <option value="0">0</option>
                                  <option value="1">1</option>
                                  <option value="3">3</option>
                               </select>
                            </div>
                        </div>
                        <div id="inner">

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <!--<input type="submit"  value="submit" class="btn btn-circle blue">-->
                                    <input type="submit" name="submit" value="submit" class="btn btn-circle blue">
                                    <button type="button" class="btn btn-circle default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <?php
    $x = $return->result();
    $count = count($x);
    if($count > 0) {
        $count_img = $x[0]->image_count;
        if ($count_img > 0)
        {
            $y = explode(',', $x[0]->image);

            if (count($y) > 1)
            {
                foreach ($y as $value)
                {
                    ?>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Add News
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <img src="<?= base_url() ?>uploads/news/<?= $value ?>"
                                         class="img-responsive center-block" style="padding: 50px; width: 50%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                } elseif(count($y) == 1)
                {
                ?>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-gift"></i>Add News
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <img src="<?= base_url() ?>uploads/news/<?= $x[0]->image ?>"
                                     class="img-responsive center-block" style="padding: 50px; width: 50%">
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            else
            {
                echo '';
            }
        }
    }
    ?>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('#image_count').change(function(){
          var no = $(this).val();
          var wrap = "";
          for(var i=1;i<=no;i++)
          {
               wrap += "<div class=\"form-group\">\n" +
                  "                            <label class=\"col-md-3 control-label\">Upload image</label>\n" +
                  "                            <div class=\"col-md-6\">\n" +
                  "                                <input type=\"file\" class=\"form-control input-circle\" placeholder=\"Enter text\" name=\"userfile[]\">\n" +
                  "\n" +
                  "                            </div>\n" +
                  "                        </div>";
          }
          $('#inner').html(wrap);
      });
    });
</script>

