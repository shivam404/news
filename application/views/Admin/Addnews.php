<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add News
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										
										<form action="<?php echo base_url('admin/addnews'); ?>" class="form-horizontal" method="post">
											<div class="form-body">
													<div class="form-group">
													<label class="col-md-3 control-label">Enter The Title</label>
													<div class="col-md-6">
														<input type="text" class="form-control input-circle" placeholder="Enter text" name="title">
														<span class="text-danger" ><?php echo form_error('title');?></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Source Link</label>
													<div class="col-md-6">
														<input type="text" class="form-control input-circle" placeholder="First Name" name="source_link">
														<span class="text-danger" ><?php echo form_error('source_link');?>
														</span>
													</div>
												</div>
												<div class="form-group">
      											<label class="col-md-3 control-label">Choose The Category</label>
      											<div class="col-md-6">
      											<select class="form-control input-circle" id="sel1" name="category">
      												<?php
      												if($cat){
															$n = 0;
														foreach($cat as $blog){
															$n++
														?>
        										<option value="<?php echo $blog->id;?>"> <?php echo $blog->category; ?></option>
        												<?php
														}
													}
												?>
      											</select>
      											<span class="text-danger" ><?php echo form_error('category');?>
														</span>

      											</div>
      											
      											</div>


      											
												
      											
      											<div class="form-group">
     											 <label class="col-md-3 control-label">Enter the Content:</label>
     											 <div class="col-md-6">
     											 <textarea class="form-control input-circle" rows="8" id="comment" name="content"></textarea>
     											 	<span class="text-danger" ><?php echo form_error('content');?>
														</span>
    											</div>
    										</div>

    									</div>
    								
    						
    									

													<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<!--<input type="submit"  value="submit" class="btn btn-circle blue">-->
														<input type="submit" name="submit" value="submit" class="btn btn-circle blue">
														<button type="button" class="btn btn-circle default">Cancel</button>
													</div>
												</div>
											</div>
										</form>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>