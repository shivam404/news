<?php
$id = $this->uri->segment(3);
if(is_numeric($id))
{
    ?>
    <a href="<?php echo base_url('Admin/edit_news_upload_image/').$id ?>" class="btn btn-success"><i class="icon-plus"></i> Add Image</a>
    <?php
}
?>
<br/>
<br/>
<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Edit News
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										
										<form action="<?php echo base_url('admin/update'); ?> " class="form-horizontal" method="post">
											<div class="form-body">
												<input type="hidden" name="txt_hidden" value="<?php echo $blog->id;?>">
													<div class="form-group">
													<label class="col-md-2 control-label">Enter The Title</label>
													<div class="col-md-10">
														<input type="text" class="form-control input-circle" placeholder="Enter text" name="title" value="<?php echo $blog->title; ?>">
														
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">Source Link</label>
													<div class="col-md-10">
														<input type="text" class="form-control input-circle" placeholder="First Name" name="source_link" value="<?php echo $blog->source_link; ?>">
														
													</div>
												</div>
												<div class="form-group">
      											<label class="col-md-2 control-label">Choose The Category</label>
      											<div class="col-md-10">
      											<select class="form-control input-circle" id="sel1" name="category">
                                                        <?php
                                                        if($cat){
                                                                $n = 0;
                                                            foreach($cat as $cate){
                                                                $n++
                                                            ?>
                                                    <option value="<?php echo $cate->id;?>"> <?php echo $cate->category; ?></option>
                                                            <?php
                                                            }
                                                        }
                                                    ?>
      											</select>
      											
      											</div>
      										
      											</div>

      											
												
      											
      											<div class="form-group">
     											 <label class="col-md-2 control-label">Enter the Content:</label>
     											 <div class="col-md-10">
     											 <textarea class="form-control input-circle ckeditor" rows="8" id="editor1" name="content"><?php echo $blog->content; ?>"</textarea>
     											 	
    										</div>

    									</div>
				
													<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<!--<input type="submit"  value="submit" class="btn btn-circle blue">-->
														<input type="submit" name="submit" value="update" class="btn btn-circle blue">
														<a href="<?php echo base_url('admin/newslist');?>" class="btn btn-circle default">Back</a>
													</div>
												</div>
											</div>
										</form>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
<script type="text/javascript">
    var editor = CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl : '<?=base_url()?>assets/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '<?=base_url()?>assets/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl : '<?=base_url()?>assets/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl : '<?=base_url()?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '<?=base_url()?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '<?=base_url()?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    CKFinder.setupCKEditor( editor, '../' );
</script>